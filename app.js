const sports = [
    {
        name: 'Football',
        bg_color: '#6bb46e',
        equipments: ['Ball', 'Football Shoes', 'Goalpost']
    },
    {
        name: 'Volleyball',
        bg_color: '#c30f42',
        equipments: ['Ball', 'Nets']
    },
    {
        name: 'Badminton',
        bg_color: '#2a82be',
        equipments: ['Shuttlecock', 'Nets', 'Racket']
    }
];

const app = Vue.createApp({
    data() {
        return {
            selectedSport: '',
            equipments: []
        };
    },
    computed: {
        selectedSportBgColor() {
            const selectedSportData = sports.find(sport => sport.name.toLowerCase() === this.selectedSport);
            return selectedSportData ? selectedSportData.bg_color : '';
        },
        selectedTransitions() {
            return 'all 1.5s ease';
        },
        selectedSportEquipments() {
            const selectedSportData = sports.find(sport => sport.name.toLowerCase() === this.selectedSport);
            return selectedSportData ? selectedSportData.equipments : [];
        },
        changePositionMe() {
            if (this.selectedSport === 'football') {
                return 'translate(-137%, -156%)'
            } else if (this.selectedSport === 'volleyball') {
                return 'translate(137%, -156%)'
            } else if (this.selectedSport === 'badminton') {
                return 'translate(0%, 156%)'
            } else {
                return "none"
            }
        }
    },
});

app.mount('#app');