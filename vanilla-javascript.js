const sports = [
    {
        name: 'Football',
        bg_color: '#6bb46e',
        equipments: ['Ball', 'Football Shoes', 'Goalpost']
    },
    {
        name: 'Volleyball',
        bg_color: '#c30f42',
        equipments: ['Ball', 'Nets']
    },
    {
        name: 'Badminton',
        bg_color: '#2a82be',
        equipments: ['Shuttlecock', 'Nets', 'Racket']
    }
];

const radioFootball = document.getElementById('radio-football');
const radioVolleyBall = document.getElementById('radio-volleyball');
const radioBadminton = document.getElementById('radio-badminton');
const meElement = document.getElementById('selected-sport-me');
const selectedSportNameElement = document.getElementById('selected-sport-name');
const equipmentListElement = document.getElementById('equipment-list');

function updateSelectedSport(sportName) {
    const selectedSport = sports.find(sport => sport.name.toLowerCase() === sportName);
    if (selectedSport) {
        // Update the background color of all sport boxes
        document.querySelectorAll('.main-border').forEach(box => {
            box.style.backgroundColor = selectedSport.bg_color;
            box.style.transition = 'all 1.5s ease';
        });

        document.querySelectorAll('.sport-box').forEach(box => {
            box.style.borderColor = 'white';
        });

        // Update selected sport name
        selectedSportNameElement.textContent = selectedSport.name;

        // Update equipment list
        equipmentListElement.innerHTML = selectedSport.equipments.map(equipment => `<li>${equipment}</li>`).join('');
    }
}

radioFootball.addEventListener('change', function () {
    meElement.style.transform = 'translate(-137%, -156%)';
    updateSelectedSport('football');
});

radioVolleyBall.addEventListener('change', function () {
    meElement.style.transform = 'translate(137%, -156%)';
    updateSelectedSport('volleyball');
});

radioBadminton.addEventListener('change', function () {
    meElement.style.transform = 'translate(0%, 156%)';
    updateSelectedSport('badminton');
});