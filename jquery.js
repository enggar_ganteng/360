const sports = [
    {
        name: 'Football',
        bg_color: '#6bb46e',
        equipments: ['Ball', 'Football Shoes', 'Goalpost']
    },
    {
        name: 'Volleyball',
        bg_color: '#c30f42',
        equipments: ['Ball', 'Nets']
    },
    {
        name: 'Badminton',
        bg_color: '#2a82be',
        equipments: ['Shuttlecock', 'Nets', 'Racket']
    }
];

$(document).ready(function () {
    const radioFootball = $('#radio-football');
    const radioVolleyBall = $('#radio-volleyball');
    const radioBadminton = $('#radio-badminton');
    const meElement = $('#selected-sport-me');
    const selectedSportNameElement = $('#selected-sport-name');
    const equipmentListElement = $('#equipment-list');

    function updateSelectedSport(sportName) {
        const selectedSport = sports.find(sport => sport.name.toLowerCase() === sportName);
        if (selectedSport) {
            // Update the background color of all sport boxes
            $('.main-border').css({
                backgroundColor: selectedSport.bg_color,
                transition: 'all 1.5s ease'
            });

            $('.sport-box').css({borderColor: 'white'});

            // Update selected sport name
            selectedSportNameElement.text(selectedSport.name);

            // Update equipment list
            equipmentListElement.html(selectedSport.equipments.map(equipment => `<li>${equipment}</li>`).join(''));
        }
    }

    radioFootball.on('change', function () {
        meElement.css('transform', 'translate(-137%, -156%)');
        updateSelectedSport('football');
    });

    radioVolleyBall.on('change', function () {
        meElement.css('transform', 'translate(137%, -156%)');
        updateSelectedSport('volleyball');
    });

    radioBadminton.on('change', function () {
        meElement.css('transform', 'translate(0%, 156%)');
        updateSelectedSport('badminton');
    });
})